TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    graph/gridlines.cpp \
    graph/gridlinesnode.cpp \
    graph/lineitem.cpp \
    graph/linenode.cpp \
    graph/pointsitem.cpp \
    graph/polygonitem.cpp \
    graph/polygonnode.cpp \
    libtess2/bucketalloc.c \
    libtess2/dict.c \
    libtess2/geom.c \
    libtess2/mesh.c \
    libtess2/priorityq.c \
    libtess2/sweep.c \
    libtess2/tess.c


RESOURCES += qml.qrc \
    graph-playground.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    graph/gridlines.h \
    graph/gridlinesnode.h \
    graph/polygonnode.h \
    graph/point.h \
    graph/polygon.h \
    graph/lineitem.h \
    graph/pointsitem.h \
    graph/polygonitem.h \
    graph/polygonnode.h

OTHER_FILES += \
    graph/shaders/gridline.vsh \
    graph/shaders/gridline.fsh
