import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import playground 0.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480

    title: qsTr("Hello World")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    RowLayout {
        anchors.fill: parent
        Rectangle {
            color: "white"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 100

            Polygon {
                id: myPolygon
                points: [
                    Qt.point(10.0, 10.0),
                    Qt.point(20.0, 210.0),
                    Qt.point(110.0, 110.0),
                    Qt.point(200.0, 210.0),
                    Qt.point(210.0, 10.0),
                    Qt.point(10.0, 10.0),

                ]
                color: "red"
                opacity: 0.5
            }

            Line {
                id: myLine
                points: myPolygon.points
                lineWidth: lineWidthSlider.value
                rotation: rotationSlider.value
                scale: scaleSlider.value
                transformOrigin: Item.Center

                Rectangle {
                    anchors.fill: parent
                    color: "red"
                    opacity: 0.2
                    antialiasing: true
                }
            }


            /*
            Line {
                points: myLine.points
                lineWidth: 1.0
                color: "green"
            }
            */

            Repeater {
                id: pointsEditor
                property Item target: myPolygon
                model: target.points.length
                delegate: Rectangle {
                    property point p: pointsEditor.target.points[index]
                    width: 7
                    height: 7
                    color: "red"
                    opacity: 0.8
                    x: p.x - 4
                    y: p.y - 4
                    onXChanged: {
                        if (mouseArea.drag.active) {
                            pointsEditor.target.setPoint(index, Qt.point(x + 4, y + 4))
                        }
                    }
                    onYChanged: {
                        if (mouseArea.drag.active) {
                            pointsEditor.target.setPoint(index, Qt.point(x + 4, y + 4))
                        }
                    }

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        drag {
                            target: parent
                        }
                    }
                }

            }
        }

        Rectangle {
            color: "grey"
            Layout.minimumWidth: 300
            Layout.fillHeight: true

            Column {
                Label {
                    text: "Line width"
                }

                Slider {
                    id: lineWidthSlider
                    width: 200
                    minimumValue: 0.0
                    maximumValue: 20.0
                    value: 5
                }
                CheckBox {
                    id: animateBox
                    text: "Animate"
                }

                Label {
                    text: "Line rotation"
                }

                Slider {
                    id: rotationSlider
                    width: 200
                    minimumValue: 0
                    maximumValue: 360
                    value: 0
                }

                Label {
                    text: "Line scale"
                }
                Slider {
                    id: scaleSlider
                    width: 200
                    minimumValue: 0
                    maximumValue: 3
                    value: 1
                }
                Button {
                    text: "Reset"
                    onClicked: {
                        animateBox.checked = false
                        lineWidthSlider.value = 5
                        rotationSlider.value = 0
                        scaleSlider.value = 1.0
                    }
                }
            }

            NumberAnimation {
                target: lineWidthSlider
                property: "value"
                from: lineWidthSlider.minimumValue
                to: lineWidthSlider.maximumValue
                loops: Animation.Infinite
                running: animateBox.checked
                duration: 5000
            }
        }
    }


}
