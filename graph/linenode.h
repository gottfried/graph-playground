/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef POLYGONNODE_H
#define POLYGONNODE_H

#include <QtQuick/QSGGeometryNode>

class QColor;

class LineNode : public QSGGeometryNode
{
public:
    LineNode();
    void setPolygon(const QPolygonF& polygon);
    void setWidth(qreal width);
    void setColor(const QColor& color);
private:
    QSGGeometry m_geometry;
};

#endif // POLYGONNODE_H
