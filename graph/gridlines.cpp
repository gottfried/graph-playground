/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "gridlines.h"

#include <QPolygonF>

#include "gridlinesnode.h"
#include "linenode.h"

GridLines::GridLines(QQuickItem* parent) :
    QQuickItem(parent),
    m_geometryChanged(false)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

void GridLines::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
    m_geometryChanged = true;
    update();
}

QSGNode* GridLines::updatePaintNode(QSGNode* oldNode, UpdatePaintNodeData*)
{
    // GridLinesNode* n = static_cast<GridLinesNode*>(oldNode);
    LineNode* n = static_cast<LineNode*>(oldNode);

    if (!n)
    {
        n = new LineNode;
        //n = new GridLinesNode();
    }

    if (m_geometryChanged)
    {
        //n->setRect(boundingRect());
        QPolygonF p;
        QRectF br(boundingRect());
        float x = br.x();
        float y = br.y();
        float w = br.width();
        float h = br.height();
        /*
        p << QPointF(x + 0.2 * w, y + 0.7 * h);
        p << QPointF(x + 0.5 * w, y + 0.3 * h);
        p << QPointF(x + 0.8 * w, y + 0.6 * h);
        */
        p << QPointF(x + 0.2 * w, y + 0.2 * h);
        p << QPointF(x + 0.5 * w, y + 0.7 * h);
        p << QPointF(x + 0.6 * w, y + 0.2 * h);
        p << QPointF(x + 0.8 * w, y + 0.5 * h);
        p << QPointF(x + 0.2 * w, y + 0.2 * h);
        n->setPolygon(p);
    }

    return n;
}
