/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "lineitem.h"

#include <QDebug>

#include "linenode.h"

LineItem::LineItem(QQuickItem* parent) :
    PointsItem(parent),
    m_lineWidth(1.0),
    m_color(Qt::black),
    m_changes(LineItem::LineWidthChanged | LineItem::ColorChanged)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

qreal LineItem::lineWidth() const
{
    return m_lineWidth;
}

void LineItem::setLineWidth(qreal lineWidth)
{
    if (lineWidth != m_lineWidth)
    {
        m_lineWidth = lineWidth;
        m_changes |= LineItem::LineWidthChanged;
        emit lineWidthChanged();
        update();
    }
}

QColor LineItem::color() const
{
    return m_color;
}

void LineItem::setColor(const QColor& color)
{
    if (color != m_color)
    {
        m_color = color;
        m_changes |= LineItem::ColorChanged;
        emit colorChanged();
        update();
    }
}

QSGNode* LineItem::updatePaintNode(QSGNode* oldNode, UpdatePaintNodeData*)
{
    LineNode* node = static_cast<LineNode*>(oldNode);

    if (!node)
    {
        node = new LineNode;
    }

    if (m_changes & LineItem::PolygonChanged)
    {

        node->setPolygon(polygon().translated(-polygonRect().x(), -polygonRect().y()));
    }

    if (m_changes & LineItem::LineWidthChanged)
    {
        node->setWidth(m_lineWidth);
    }

    if (m_changes & LineItem::ColorChanged)
    {
        node->setColor(m_color);
    }

    m_changes = LineItem::NoChanges;

    return node;
}

void LineItem::polygonChanged(const QPolygonF& newPolygon)
{
    PointsItem::polygonChanged(newPolygon);
    m_changes |= LineItem::PolygonChanged;
    update();
}
