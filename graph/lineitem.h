/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LINEITEM_H
#define LINEITEM_H

#include "pointsitem.h"

#include <QPolygonF>

class LineItem : public PointsItem
{
    Q_OBJECT
    Q_PROPERTY(qreal lineWidth READ lineWidth WRITE setLineWidth NOTIFY lineWidthChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
public:
    enum LineItemChange {
        NoChanges = 0x0,
        PolygonChanged = 0x1,
        LineWidthChanged = 0x2,
        ColorChanged = 0x4
    };
    Q_DECLARE_FLAGS(LineItemChanges, LineItemChange)
    explicit LineItem(QQuickItem* parent = 0);
    qreal lineWidth() const;
    void setLineWidth(qreal lineWidth);
    QColor color() const;
    void setColor(const QColor& color);
signals:
    void lineWidthChanged();
    void colorChanged();
protected:
    QSGNode* updatePaintNode(QSGNode*, UpdatePaintNodeData*);
    void polygonChanged(const QPolygonF& newPolygon);
private:
    qreal m_lineWidth;
    QColor m_color;
    LineItemChanges m_changes;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(LineItem::LineItemChanges)

#endif // LINEITEM_H
