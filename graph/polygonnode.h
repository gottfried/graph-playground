/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef POLYGONNODE_H
#define POLYGONNODE_H

#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

class PolygonNode : public QSGGeometryNode
{
public:
    PolygonNode();
    void setPolygon(const QPolygonF& polygon);
    void setColor(const QColor& color);
private:
    static void* alloc(void* userData, unsigned int size);
    static void* realloc(void* userData, void* ptr, unsigned int size);
    static void free(void* userData, void* ptr);
    QSGGeometry m_geometry;
    QSGFlatColorMaterial m_material;
};

#endif // POLYGONNODE_H
