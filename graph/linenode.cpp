/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "linenode.h"

#include "math.h"

#include <QtQuick/QSGSimpleMaterialShader>

#include <QColor>
#include <QPolygonF>

QPointF normal(const QPointF& vec)
{
    return QPointF(vec.y(), -vec.x());
}

float length(const QPointF& vec)
{
    return sqrt(vec.x() * vec.x() + vec.y() * vec.y());
}

QPointF normalize(const QPointF& vec)
{
    float l = length(vec);
    return QPointF(vec.x() / l, vec.y() / l);
}

struct LineMaterial
{
    QColor color;
    float halfwidth;
    float feather;
    float length;
    bool closed;
};

class LineShader : public QSGSimpleMaterialShader<LineMaterial>
{
    QSG_DECLARE_SIMPLE_SHADER(LineShader, LineMaterial)

public:
    LineShader() {
        setShaderSourceFile(QOpenGLShader::Vertex, ":/graph/shaders/line.vsh");
        setShaderSourceFile(QOpenGLShader::Fragment, ":/graph/shaders/line.fsh");
    }

    QList<QByteArray> attributes() const {  return QList<QByteArray>() << "pos" << "normal" << "p" << "t"; }

    void updateState(const LineMaterial *m, const LineMaterial *) {
        program()->setUniformValue(id_color, m->color);
        program()->setUniformValue(id_halfwidth, m->halfwidth);
        program()->setUniformValue(id_feather, m->feather);
        program()->setUniformValue(id_length, m->length);
        program()->setUniformValue(id_closed, m->closed);
    }

    void resolveUniforms() {
        id_color = program()->uniformLocation("color");
        id_halfwidth = program()->uniformLocation("halfwidth");
        id_feather = program()->uniformLocation("feather");
        id_length = program()->uniformLocation("length");
        id_closed = program()->uniformLocation("closed");
    }

private:
    int id_color;
    int id_halfwidth;
    int id_feather;
    int id_length;
    int id_closed;
};


struct LineVertex {
    float x;
    float y;
    float nx;
    float ny;
    float p;
    float t;
    inline void set(float xx, float yy, float nxx, float nyy, float pp, float tt) { x = xx; y = yy; nx = nxx; ny = nyy; p = pp; t = tt; }
};

static const QSGGeometry::AttributeSet &attributes()
{
    static QSGGeometry::Attribute attr[] = {
        QSGGeometry::Attribute::create(0, 2, GL_FLOAT, true),
        QSGGeometry::Attribute::create(1, 2, GL_FLOAT),
        QSGGeometry::Attribute::create(2, 1, GL_FLOAT),
        QSGGeometry::Attribute::create(3, 1, GL_FLOAT)
    };
    static QSGGeometry::AttributeSet set = { 3, 6 * sizeof(float), attr };
    return set;
}

LineNode::LineNode() :
    m_geometry(attributes(), 0)
{
    setGeometry(&m_geometry);
    m_geometry.setDrawingMode(GL_TRIANGLES);

    QSGSimpleMaterial<LineMaterial> *m = LineShader::createMaterial();
    m->setFlag(QSGMaterial::RequiresFullMatrixExceptTranslate);
    m->state()->halfwidth = 3.5;
    m->state()->color = Qt::black;
    m->state()->feather = 0.5;
    m->setFlag(QSGMaterial::Blending, true);

    setMaterial(m);
    setFlag(OwnsMaterial, true);
}

void LineNode::setPolygon(const QPolygonF& polygon)
{
    int lineSegments = polygon.length() - 1;
    int vertexCount = 2 * 3 * lineSegments; // two triangles per segment

    geometry()->allocate(vertexCount);

    if (lineSegments < 1)
    {
        return;
    }

    bool closed = polygon[0] == polygon[lineSegments];

    LineVertex* v = static_cast<LineVertex*>(geometry()->vertexData());

    QPointF previousDir, nextDir;
    QPointF previousNormal, nextNormal;
    QPointF previousJoinExtrusion;
    float previousJoinAngularity;
    float positionOnLine = 0;

    // number of caps + joins
    int corners = lineSegments + 1;

    nextDir = closed? polygon[0] - polygon[lineSegments - 1]: polygon[1] - polygon[0];
    nextNormal = normalize(normal(nextDir));


    for (int i = 0; i < corners; i++)
    {
        previousDir = nextDir;
        previousNormal = nextNormal;

        if (i == corners - 1)
        {
            nextDir = closed? polygon[1] - polygon[0]: previousDir;
        }
        else
        {
            nextDir = polygon[i + 1] - polygon[i];
        }

        nextNormal = normalize(normal(nextDir));

        QPointF joinExtrusion = normalize(previousNormal + nextNormal);
        qreal joinAngularity = QPointF::dotProduct(previousNormal, joinExtrusion);
        joinAngularity = isnan(joinAngularity)? 0: joinAngularity;
        joinExtrusion /= joinAngularity;

        if (i > 0)
        {
            const QPointF a = polygon[i - 1];
            const QPointF b = polygon[i];
            const QPointF extrusianA = previousJoinAngularity < 0.2? previousNormal: previousJoinExtrusion;
            const QPointF extrusianB = joinAngularity < 0.2? previousNormal: joinExtrusion;

            const float segmentLength = length(b - a);
            const float positionOnLineA = positionOnLine;
            const float positionOnLineB = positionOnLine + segmentLength;

            // first triangle
            (v++)->set(a.x(), a.y(), extrusianA.x(), extrusianA.y(), positionOnLineA, -1);
            (v++)->set(b.x(), b.y(), extrusianB.x(), extrusianB.y(), positionOnLineB, -1);
            (v++)->set(a.x(), a.y(), extrusianA.x(), extrusianA.y(), positionOnLineA, 1);

            // second triangle
            (v++)->set(b.x(), b.y(), extrusianB.x(), extrusianB.y(), positionOnLineB, -1);
            (v++)->set(a.x(), a.y(), extrusianA.x(), extrusianA.y(), positionOnLineA, 1);
            (v++)->set(b.x(), b.y(), extrusianB.x(), extrusianB.y(), positionOnLineB, 1);

            positionOnLine = positionOnLineB;
        }

        previousJoinExtrusion = joinExtrusion;
        previousJoinAngularity = joinAngularity;
    }

    markDirty(QSGNode::DirtyGeometry);

    QSGSimpleMaterial<LineMaterial>* m = static_cast<QSGSimpleMaterial<LineMaterial>*>(material());
    m->state()->length = positionOnLine;
    m->state()->closed = closed;
    markDirty(QSGNode::DirtyMaterial);
}

void LineNode::setWidth(qreal width)
{
    QSGSimpleMaterial<LineMaterial>* m = static_cast<QSGSimpleMaterial<LineMaterial>*>(material());
    m->state()->halfwidth = width / 2;
    // hair lines look better with an increased feather
    m->state()->feather = width < 1.0? 0.75 - (width / 4): 0.5;
    markDirty(QSGNode::DirtyMaterial);
}

void LineNode::setColor(const QColor& color)
{
    QSGSimpleMaterial<LineMaterial>* m = static_cast<QSGSimpleMaterial<LineMaterial>*>(material());
    m->state()->color = color;
    markDirty(QSGNode::DirtyMaterial);
}
