/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "gridlinesnode.h"

#include <QColor>

#include <QtQuick/QSGSimpleMaterial>
#include <QtQuick/QSGFlatColorMaterial>

#define GRID_SIZE 32

struct DashedGridLineMaterial {
    QColor color;
    float dashLength;
};

class DashedLineShader: public QSGSimpleMaterialShader<DashedGridLineMaterial>
{
    QSG_DECLARE_SIMPLE_SHADER(DashedLineShader, DashedGridLineMaterial)
public:
    DashedLineShader()
    {
        setShaderSourceFile(QOpenGLShader::Vertex, ":/graph/shaders/gridline.vsh");
        setShaderSourceFile(QOpenGLShader::Fragment, ":/graph/shaders/gridline.fsh");
    }

    QList<QByteArray> attributes() const
    {
        return QList<QByteArray>() << "aVertex";
    }

    void updateState(const DashedGridLineMaterial* m, const DashedGridLineMaterial*)
    {
        program()->setUniformValue(id_color, m->color);
        program()->setUniformValue(id_dashLength, m->dashLength);
    }

    void resolveUniforms()
    {
        id_color = program()->uniformLocation("uColor");
        id_dashLength = program()->uniformLocation("uDashLength");
    }

private:
    int id_color;
    int id_dashLength;
};

GridLinesNode::GridLinesNode() :
    m_geometry(QSGGeometry::defaultAttributes_Point2D(), 0)
{
    setGeometry(&m_geometry);
    m_geometry.setDrawingMode(GL_LINES);
    m_geometry.setLineWidth(1.0);

    QSGSimpleMaterial<DashedGridLineMaterial>* m = DashedLineShader::createMaterial();
    m->state()->color = Qt::gray;
    m->state()->dashLength = 8.0;
    m->setFlag(QSGMaterial::Blending);

    /*
    QSGFlatColorMaterial *m = new QSGFlatColorMaterial;
    m->setColor(Qt::gray);
    */

    setMaterial(m);
    setFlag(OwnsMaterial, true);
    //setMaterial(&m_material);
    //m_material.setColor(Qt::gray);
}

void GridLinesNode::setRect(const QRectF& rect)
{
    const int lineCount = int((rect.height() - 1)) / GRID_SIZE;

    QSGGeometry *g = geometry();

    geometry()->allocate(2 * lineCount);

    const float x = rect.x();
    const float y = rect.y();
    const float w = rect.width();
    const float h = rect.height();

    QSGGeometry::Point2D *v = g->vertexDataAsPoint2D();

    for (int i = 0; i < lineCount; i++)
    {
        qreal iy =  h - y - (i + 1) * GRID_SIZE;
        v[2 * i].set(x, iy);
        v[2 * i + 1].set(x + w, iy);
    }

    markDirty(QSGNode::DirtyGeometry);
}
