/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "polygonnode.h"

#include <memory.h>

#include <QPolygonF>

#include "libtess2/tesselator.h"

PolygonNode::PolygonNode() :
    m_geometry(QSGGeometry::defaultAttributes_Point2D(), 0)
{
    setGeometry(&m_geometry);
    m_geometry.setDrawingMode(GL_TRIANGLES);

    setMaterial(&m_material);
}

void PolygonNode::setPolygon(const QPolygonF& polygon)
{
    const int points = polygon.length();
    const int verticesPerGroup = 3;
    static TESSalloc allocator = {
        &alloc, &realloc, &free,
        0, // userData
        64,                               // meshEdgeBucketSize
        64,                               // meshVertexBucketSize
        32,                               // meshFaceBucketSize
        64,                               // dictNodeBucketSize
        8,                                // regionBucketSize
        128, // extraVertices allocated for the priority queue
    };

    TESStesselator* tess = tessNewTess(&allocator);

    QVector< QPair<TESSreal,TESSreal> > tesselationData(points);

    for (int i = 0; i < points; i++)
    {
        tesselationData[i] = qMakePair(polygon[i].x(), polygon[i].y());
    }

    tessAddContour(tess, 2, &tesselationData.first(), 2 * sizeof(TESSreal), polygon.length());
    tessTesselate(tess, TESS_WINDING_ODD, TESS_POLYGONS, verticesPerGroup, 2, 0);

    const int elementCount = tessGetElementCount(tess);
    const TESSindex* elements = tessGetElements(tess);
    const TESSreal* vertices = tessGetVertices(tess);
    int triangleCount = 0;


    for (int i = 0; i < elementCount; i++)
    {
        const TESSindex* element_group = &elements[i * verticesPerGroup];

        const TESSindex indexA = element_group[0];
        const TESSindex indexB = element_group[1];
        const TESSindex indexC = element_group[2];

        if (indexA != TESS_UNDEF && indexB != TESS_UNDEF && indexC != TESS_UNDEF)
        {
            triangleCount++;
        }

        qDebug() << i << indexA << indexB << indexC;
    }

    QSGGeometry* g = geometry();
    g->allocate(3 * triangleCount);
    QSGGeometry::Point2D* v = g->vertexDataAsPoint2D();

    for (int i = 0; i < elementCount; i++)
    {
        const TESSindex* element_group = &elements[i * verticesPerGroup];

        const TESSindex indexA = element_group[0];
        const TESSindex indexB = element_group[1];
        const TESSindex indexC = element_group[2];

        if (indexA != TESS_UNDEF && indexB != TESS_UNDEF && indexC != TESS_UNDEF)
        {
            (v++)->set((float)vertices[indexA * 2], (float)vertices[indexA * 2 + 1]);
            (v++)->set((float)vertices[indexB * 2], (float)vertices[indexB * 2 + 1]);
            (v++)->set((float)vertices[indexC * 2], (float)vertices[indexC * 2 + 1]);
            qDebug() << i << "A" << vertices[indexA * 2] << vertices[indexA * 2 + 1];
            qDebug() << i << "B" << vertices[indexB * 2] << vertices[indexB * 2 + 1];
            qDebug() << i << "C" << vertices[indexC * 2] << vertices[indexC * 2 + 1];
        }

        QSGGeometry::Point2D* v = g->vertexDataAsPoint2D();
    }

    markDirty(QSGNode::DirtyGeometry);
    tessDeleteTess(tess);
}

void PolygonNode::setColor(const QColor& color)
{
    m_material.setColor(color);
    markDirty(QSGNode::DirtyMaterial);
}

void* PolygonNode::alloc(void* userData, unsigned int size)
{
    Q_UNUSED(userData);
    return ::malloc(size);
}

void* PolygonNode::realloc(void* userData, void* ptr, unsigned int size)
{
    Q_UNUSED(userData);
    return ::realloc(ptr, size);
}

void PolygonNode::free(void *userData, void *ptr)
{
    Q_UNUSED(userData);
    return ::free(ptr);
}
