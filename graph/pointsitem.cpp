/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pointsitem.h"

PointsItem::PointsItem(QQuickItem* parent) :
    QQuickItem(parent)
{
}

QVariantList PointsItem::points() const
{
    QVariantList points;
    foreach(const QPointF point, m_polygon)
    {
        points << point;
    }
    return points;
}

void PointsItem::setPoints(const QVariantList& points)
{
    QPolygonF newPolygon = m_polygon;
    bool changed = false;
    if (points.length() != newPolygon.length())
    {
        newPolygon.resize(points.length());
    }
    for (int i = 0; i < newPolygon.length(); i++)
    {
        const QPointF point = points[i].toPointF();
        if (point != newPolygon[i])
        {
            newPolygon[i] = point;
            changed = true;
        }
    }
    if (changed)
    {
        polygonChanged(newPolygon);
    }
}

void PointsItem::setPoint(int index, const QPointF& point)
{
    if (index >= m_polygon.length())
    {
        return;
    }

    if (point != m_polygon[index])
    {
        m_polygon[index] = point;
        polygonChanged(m_polygon);
    }
}

QPolygonF PointsItem::polygon() const
{
    return m_polygon;
}

void PointsItem::setPolygon(const QPolygonF polygon)
{
    if (polygon != m_polygon)
    {
        polygonChanged(m_polygon);
    }
}

QRectF PointsItem::polygonRect() const
{
    return m_polygonRect;
}

void PointsItem::polygonChanged(const QPolygonF &newPolygon)
{
    m_polygon = newPolygon;
    m_polygonRect = m_polygon.boundingRect();
    setX(m_polygonRect.x());
    setY(m_polygonRect.y());
    setWidth(m_polygonRect.width());
    setHeight(m_polygonRect.height());
    emit pointsChanged();
}
