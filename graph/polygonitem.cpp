/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "polygonitem.h"

#include "polygonnode.h"

PolygonItem::PolygonItem(QQuickItem* parent) :
    PointsItem(parent),
    m_color(Qt::black),
    m_changes(PolygonItem::ColorChanged)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

QColor PolygonItem::color() const
{
    return m_color;
}

void PolygonItem::setColor(const QColor& color)
{
    if (color != m_color)
    {
        m_color = color;
        m_changes |= PolygonItem::ColorChanged;
        emit colorChanged();
        update();
    }
}

QSGNode* PolygonItem::updatePaintNode(QSGNode* oldNode, UpdatePaintNodeData*)
{
    PolygonNode* node = static_cast<PolygonNode*>(oldNode);

    if (!node)
    {
        node = new PolygonNode;
    }

    if (m_changes & PolygonItem::PolygonChanged)
    {

        node->setPolygon(polygon().translated(-polygonRect().x(), -polygonRect().y()));
    }

    if (m_changes & PolygonItem::ColorChanged)
    {
        node->setColor(m_color);
    }

    m_changes = PolygonItem::NoChanges;

    return node;
}

void PolygonItem::polygonChanged(const QPolygonF& newPolygon)
{
    PointsItem::polygonChanged(newPolygon);
    m_changes |= PolygonItem::PolygonChanged;
    update();
}
