uniform lowp float qt_Opacity;
uniform lowp vec4 uColor;
uniform highp float uDashLength;

varying highp vec4 vVertex;

void main()
{
    highp float p = mod(vVertex.x, 2.0 * uDashLength);
    lowp float opacity = step(uDashLength, p) * qt_Opacity;
    gl_FragColor = uColor * qt_Opacity;
}
