attribute highp vec4 pos;
attribute highp vec2 normal;
attribute highp float p;
attribute highp float t;

uniform lowp float halfwidth;
uniform lowp float feather;
uniform highp mat4 qt_Matrix;
uniform highp float length;
uniform bool closed;

varying lowp float d;
varying lowp float positionOnLine;

void main(void)
{
    vec4 adjustedPos = pos;
    lowp float effectiveWidth = halfwidth + feather;

    // extrude the vertex from line center along the passed extrude vector
    adjustedPos += vec4(normal.xy, 0, 0) * t * effectiveWidth;

    d = effectiveWidth * t;
    positionOnLine = p;

    // sqare caps for open line strings
    if (!closed)
    {
        // at line caps, the passed extrude vector is perpendicular to the line segemnt,
        // so we can use it the to extend the line to form the caps
        if (p == 0.0)
        {
            positionOnLine -= effectiveWidth;
            adjustedPos += vec4(normal.y, -normal.x, 0, 0) * effectiveWidth;
        }
        else if (p == length)
        {
            positionOnLine += effectiveWidth;
            adjustedPos += vec4(-normal.y, normal.x, 0, 0) * effectiveWidth;
        }
    }

    gl_Position = qt_Matrix * adjustedPos;
}
