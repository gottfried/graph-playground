uniform lowp vec4 color;
uniform lowp float halfwidth;
uniform lowp float feather;
uniform highp float length;
uniform bool closed;

uniform lowp float qt_Opacity;

varying lowp float d;
varying lowp float positionOnLine;

void main(void)
{
    // antialisaing for the line sides: calculate distance from the line center, fragmentes in the feather
    // become an opacity in [1.0, 0.0]
    lowp float opacity = qt_Opacity * (1.0 - smoothstep(halfwidth - feather, halfwidth + feather, abs(d)));

    // Square caps and their antialiasing for open line strings
    if (!closed)
    {
        opacity *= min(
                    smoothstep(-halfwidth - feather, -halfwidth + feather, positionOnLine),
                    1.0 - smoothstep(length + halfwidth - feather, length + halfwidth + feather, positionOnLine));
    }

    gl_FragColor = color * opacity;
}
