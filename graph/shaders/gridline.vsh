attribute highp vec4 aVertex;

uniform highp mat4 qt_Matrix;

varying highp vec4 vVertex;

void main(void)
{
    gl_Position = qt_Matrix * aVertex;
    vVertex = aVertex;
}
