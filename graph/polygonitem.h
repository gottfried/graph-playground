/*
 * Copyright 2014  Sebastian Gottfried <sebastiangottfried@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POLYGONITEM_H
#define POLYGONITEM_H

#include "pointsitem.h"

class PolygonItem : public PointsItem
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
public:
    explicit PolygonItem(QQuickItem *parent = 0);
    enum PolygonItemChange {
        NoChanges = 0x0,
        PolygonChanged = 0x1,
        ColorChanged = 0x2
    };
    Q_DECLARE_FLAGS(PolygonItemChanges, PolygonItemChange)
    QColor color() const;
    void setColor(const QColor& color);
signals:
    void colorChanged();
protected:
    QSGNode* updatePaintNode(QSGNode*, UpdatePaintNodeData*);
    void polygonChanged(const QPolygonF& newPolygon);
private:
    QColor m_color;
    PolygonItemChanges m_changes;
};

#endif // POLYGONITEM_H
