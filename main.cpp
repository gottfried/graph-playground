#include <QApplication>
#include <QQmlApplicationEngine>

#include "graph/gridlines.h"
#include "graph/lineitem.h"
#include "graph/polygonitem.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<GridLines>("playground", 0, 1, "GridLines");
    qmlRegisterType<PolygonItem>("playground", 0, 1, "Polygon");
    qmlRegisterType<LineItem>("playground", 0, 1, "Line");

     QQmlApplicationEngine engine;
     engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
